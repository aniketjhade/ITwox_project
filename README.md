How to run the code.

1)open folder ITwox project in any code editor(My_todo_app folder)

3)go to client and server path and install all the dependencies using npm install command. 
(if required npm install --force)

4)after all the dependencies added run npm start on both client and server.

5)you will be redirected to browser where app is running.

features added in the applicatin:

1)user can signIn, login , logout.

2)Implemented REST api for calling provided api to show data.

3)created dashboard and home page to display response data from api.
